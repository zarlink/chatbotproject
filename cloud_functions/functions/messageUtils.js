

async function getYesNoSimpleCard(question,positiveMsg,negMsg){

    return  {
        card: {
          subtitle: question,
          buttons: [
            {
              text: positiveMsg,
              postback: "Yes"
            },
            {
              text: negMsg,
              postback: "no"
            }
          ]
        },
      }

}


module.exports = {
    getYesNoSimpleCard
}