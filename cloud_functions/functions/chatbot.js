const functions = require('firebase-functions');
const admin = require('firebase-admin');
const axios = require('axios');
const {WebhookClient, Payload} = require('dialogflow-fulfillment');
const fs = require('fs');
const { globalAgent } = require('http');
const db = admin.firestore();
const globalThis = this;

/** parameters for temp saving  */

globalThis.rooms = ['Single','King','GroupRoom1'];


/** This function must return true or false, in case if a room is avalaible or not.  */
async function checkAvalaibleDate(){
  console.log("Revisará disponibilidad de fechas. Cuartos a revisar: "+globalThis.amountRooms);
  //Si la cantidad de piezas es 1, revisa en cama escogida.
  if(globalThis.amountRooms == 1){
    var hotelRooms = admin.firestore().collection("ChoripanHotel/"+globalThis.bed+"/dates/");  
  } 
    else { //Caso contrario, parte por single.
      var camaSimple = globalThis.rooms[0];
      var hotelRooms = admin.firestore().collection("ChoripanHotel/"+camaSimple+"/dates/");  
  }
  
  //var documentIds = ['session', 'session2']; //Esto debe ser obtenido por una variable global
  const queryRef = hotelRooms.where(admin.firestore.FieldPath.documentId(), 'in', globalThis.daysArraysForInsert);
  //console.log("Resultado consulta: "+JSON.stringify(queryRef));
  var check = await queryRef.get().then((doc) => {
        console.log("Total de elementos obtenidos: "+doc.size); //+" nombre: "+JSON.stringify(doc.data()));
        var totalMatches = 0;
        doc.forEach(doc2 => {
          if(globalThis.daysArraysForInsert.includes(doc2.id) == true){
            totalMatches += 1
            console.log("Las piezas ya estan ocupadas para los dias: "+globalThis.daysArraysForInsert)
            return false;  
          } 
        });
        
        if(totalMatches == 0){
          console.log("Hay fechas disponibles, intentará insertar");
            return true;
        } else{
          console.log("Las piezas ya estan ocupadas para los dias: "+globalThis.daysArraysForInsert)
            return false;
        }
      });
      return check;
}

/** This function must create an array Of Dates to be inserted on The database. */
async function createListOfDatesToUpdate(){
  console.log("Entra a funcion que creara lista de dias");
  
        var amountDays = globalThis.days;
        var arrivalDate = globalThis.dateArrival
        var arrayWithDates =[]
        var dateAsEpoch = Date.parse(arrivalDate);
        
        arrayWithDates.push(arrivalDate.split('T')[0]);
        for (let i = 1; i <= amountDays; i++) {
          var newDayEpoch = dateAsEpoch + 86400*1000*i;
          var newDate = new Date(newDayEpoch).toISOString();
          arrayWithDates.push(newDate.split('T')[0]);
        } 
        console.log("Los dias a insertar serán: "+JSON.stringify(arrayWithDates));
        globalThis.daysArraysForInsert = arrayWithDates;
        return true;

  
}

/** If everything it's OK, this function will copy the content from session2, to the respective room.  */
async function saveRoomsInHotel(){
  console.log("dentro de funcion encargada de guardar datos en BD");

  var daysForInsert = globalThis.daysArraysForInsert;
  var cuartosreservados = globalThis.amountRooms;
  console.log("Cuartos reservados: "+cuartosreservados+ "Dias a guardar: "+daysForInsert+" Adicional: "+JSON.stringify(daysForInsert));

  if(cuartosreservados == 1){
    var dias = daysForInsert.length;
    console.log("Dentro de opcion que confirma 1 cuarto. Dias que guardará: "+dias);
      
      for (let i = 0; i < dias; i++) {
        console.log("Dentro de loop de dias. guardara el dia: "+daysForInsert[i]);
        var value = {
            name: globalThis.name+" "+globalThis.surname,
            dateArrival : globalThis.dateArrival,
            amountOfPeople : globalThis.amountOfPeople,
            bedType: globalThis.cama,
            amountOfRooms: globalThis.amountRooms,
            restingDays: globalThis.days,
            breakfast: globalThis.breakfast,
            paymentMethod: globalThis.paymentMethod

        }

        var currentDay = daysForInsert[i];
        console.log("Guardara los datos del dia: "+currentDay+" en la pieza: "+globalThis.cama)
       
        /** If the amount of rooms is 0, assign the selected one, other case... all the rooms. */
        var roomData = admin.firestore().collection("ChoripanHotel/"+globalThis.cama+"/dates").doc(currentDay);  
        await roomData.set(value,{merge: true})
      }
      return true;
  
  } else {
    console.log("Dentro de opcion que confirma mas de 1 cuarto");
    /** this code can be optimized */
    for (let bedroom = 0; bedroom < cuartosreservados; bedroom++){
      var dias = daysForInsert.length;
      for (let i = 0; i < dias; i++) {

        var value = {
            name: globalThis.name+" "+globalThis.surname,
            dateArrival : globalThis.dateArrival,
            amountOfPeople : globalThis.amountOfPeople,
            bedType: globalThis.cama ?? "regular",
            amountOfRooms: globalThis.amountRooms,
            restingDays: globalThis.days,
            breakfast: globalThis.breakfast,
            paymentMethod: globalThis.paymentMethod

        }

        var currentDay = daysForInsert[i];
        var currentBed = globalThis.rooms[bedroom]
        console.log("Guardara los datos del dia: "+currentDay+" en la pieza: "+currentBed)
       
        /** If the amount of rooms is 0, assign the selected one, other case... all the rooms. */
        var roomData = admin.firestore().collection("ChoripanHotel/"+currentBed+"/dates").doc(currentDay);  
        await roomData.set(value,{merge: true})
      }
    
  }
  
    
    return true;
  }
}



exports.choripanChatBot = functions.https.onRequest(async (request, response) => {
    const tag = request.body.queryResult.intent.displayName;
  
    let jsonResponse = {};
    if (tag === '1_Default Welcome Intent') {

      jsonResponse = setCardWithImageQuestion("Welcome to Choripan Hotel!",
                  "Do you want to make a reservation?","https://www.pinnaclepierhotel.com/Content/images/hotel-north-vancouver.jpg", "Yes", "No");
      
      
    } else if (tag === '2_B_Appointment - No') {
      
        jsonResponse = getYesNoSimpleQuestion("Do you want to see availability for any particular date?",
    "Sure", "No. I'm good.");
      
    } else if(tag === 'Avalaibility - No - Finish'){
        jsonResponse = setSimpleText("That's fine... If you want to see more info, don't forget to check our website: https://tinyurl.com/3s6v4hm6");
    } 
    else if(tag === 'Avalaibility - Yes'){
      let dateSelected = request.body.queryResult.parameters['dateCheck'];
  
      jsonResponse = getYesNoSimpleQuestion("There are avalaibility for day: "+dateSelected+". Now... do you want to make a reservation?"
      ,"Sure", "No, it's ok.");
    }
    else if(tag === '2_A_Appointment - Yes'){
      
      jsonResponse = setSimpleText("Excellent "+name+" "+surname+". Now write the day that you want to arrive ");
    } 
    else if(tag === '3_Date-Arrival - askForPeople'){
      
      jsonResponse = setSimpleText("So, for the day "+globalThis.dateArrival.split('T')[0]+" tell us, how many additional people will come with you?");
    }
    else if(tag === '4_SetAmountPeople - AskForRooms'){
      
      jsonResponse = getYesNoSimpleQuestion("Do you need more than one room? ", "Yes","No. Only one");
    }
    else if(tag === '5_B_SingleRoom - askForBed'){
      
      jsonResponse = setCardWithImageOption("","Excelent, now tell us, what kind of bed do you prefer?"
      ,"https://tinyurl.com/4kbbcfte","Single", "King");
    } 
    else if(tag === '5_A_MultipleRooms - askRooms'){
      
      jsonResponse = setSimpleText("How many rooms do you need? (Max is 3. So if you write a greater value, 3 will be assigned.)");
    } 
    else if(tag === '6_B_BedSelected - AskManyDaysStays'){
    
        var cama = request.body.queryResult.parameters['bedtype'];
      
        jsonResponse = setSimpleText("Good choice the "+cama+" bed. How many days do you want to stay?");        
  
    }
    else if(tag === '6_SetRooms - AskManyDaysStays'){
    
      jsonResponse = setSimpleText("Having the amount of rooms solved, How many days do you want to stay?");        

  }
    else if(tag === '7_B_SetDaysFromBed - RequestBreakfast'){
      
      jsonResponse = setCardWithImageQuestion("We are almost done!",
      "Do you want to include breakfast?","https://i.stack.imgur.com/qVLgQ.jpg", "Yes", "No");
    }
    else if(tag === '7_SetDays - RequestBreakfast'){
      
      jsonResponse = setCardWithImageQuestion("We are almost done!",
      "Do you want to include breakfast?","https://i.stack.imgur.com/qVLgQ.jpg", "Yes", "No");
    }
    else if(tag === 'setBreakfast - AskPaymentMethod_2'){
    
      jsonResponse = setCardWithImageOption("About payment",
      "When you get to our Hotel, how do you will pay?","https://tinyurl.com/4zfhdprf", "Credit/Debit", "Money Bills");
    }
    else if(tag === 'setPayment - AskReminder_2'){
    
      jsonResponse = setCardWithImageQuestion("We are done!",
      "do you wish to be reminded?","", "Yes please :)", "No, I'm fine");
    }
    else if(tag === 'SaveInfo - finish'){
  
      globalThis.name = request.body.queryResult.outputContexts[2].parameters.name ?? request.body.queryResult.outputContexts[3].parameters.name; // parameters['name'];
      globalThis.surname = request.body.queryResult.outputContexts[2].parameters['surname'] ?? request.body.queryResult.outputContexts[3].parameters['surname'];//request.body.queryResult.parameters['surname'];
      globalThis.dateArrival = request.body.queryResult.outputContexts[2].parameters['arrivalDate'] ?? request.body.queryResult.outputContexts[3].parameters['arrivalDate'];
      globalThis.amountOfPeople = request.body.queryResult.outputContexts[2].parameters['amountpeople'] ?? request.body.queryResult.outputContexts[3].parameters['amountpeople'];
      globalThis.cama = request.body.queryResult.outputContexts[2].parameters['bedtype'] ?? request.body.queryResult.outputContexts[3].parameters['bedtype'];
      var pieza = request.body.queryResult.outputContexts[2].parameters['rooms'] ?? request.body.queryResult.outputContexts[3].parameters['rooms'];
      
      switch(pieza){
        case pieza < 0: 
            pieza = 0;
            break;
        case pieza > 3:
            pieza = 3;
            break;
      }
      globalThis.amountRooms = pieza;
      console.log("Cantidad de piezas confirmadas: "+globalThis.amountRooms);
      globalThis.days = request.body.queryResult.outputContexts[2].parameters['days'] ?? request.body.queryResult.outputContexts[3].parameters['days'];
      globalThis.breakfast = request.body.queryResult.outputContexts[2].parameters['breakfast'] ?? request.body.queryResult.outputContexts[3].parameters['breakfast'];
      globalThis.paymentMethod = request.body.queryResult.outputContexts[2].parameters['paymentMethod'] ?? request.body.queryResult.outputContexts[3].parameters['paymentMethods'];

      var lastOp = await finalStepFunction();
      if(lastOp == true){
        jsonResponse = setSimpleText(globalThis.finalmessage);
      }

    }
    else if(tag === 'askEmail-setInfo-finish'){
      console.log("askEmail-setInfo-finish"+JSON.stringify(request.body))
      globalThis.mail = request.body.queryResult.parameters['email'];
      globalThis.name = request.body.queryResult.outputContexts[2].parameters.name ?? request.body.queryResult.outputContexts[3].parameters.name; // parameters['name'];
      globalThis.surname = request.body.queryResult.outputContexts[2].parameters['surname'] ?? request.body.queryResult.outputContexts[3].parameters['surname'];//request.body.queryResult.parameters['surname'];
      globalThis.dateArrival = request.body.queryResult.outputContexts[2].parameters['arrivalDate'] ?? request.body.queryResult.outputContexts[3].parameters['arrivalDate'];
      globalThis.amountOfPeople = request.body.queryResult.outputContexts[2].parameters['amountpeople'] ?? request.body.queryResult.outputContexts[3].parameters['amountpeople'];
      globalThis.cama = request.body.queryResult.outputContexts[2].parameters['bedtype'] ?? request.body.queryResult.outputContexts[3].parameters['bedtype'];
      var pieza = request.body.queryResult.outputContexts[2].parameters['rooms'] ?? request.body.queryResult.outputContexts[3].parameters['rooms'];
      
      switch(pieza){
        case pieza < 0: 
            pieza = 0;
            break;
        case pieza > 3:
            pieza = 3;
            break;
      }
      globalThis.amountRooms = pieza;
      globalThis.days = request.body.queryResult.outputContexts[2].parameters['days'] ?? request.body.queryResult.outputContexts[3].parameters['days'];
      globalThis.breakfast = request.body.queryResult.outputContexts[2].parameters['breakfast'] ?? request.body.queryResult.outputContexts[3].parameters['breakfast'];
      globalThis.paymentMethod = request.body.queryResult.outputContexts[2].parameters['paymentMethod'] ?? request.body.queryResult.outputContexts[3].parameters['paymentMethods'];

      
      var lastOp = await finalStepFunction();
      if(lastOp == true){
        jsonResponse = setSimpleText(globalThis.finalmessage);
      }
    
      
    }
    

    /** Ultimo tag */
    else {
      jsonResponse = setSimpleText(`There are no fulfillment responses defined for "${tag}"" tag`)
    }
    response.send(jsonResponse);
  });


  async function finalStepFunction(){
    var createList = await createListOfDatesToUpdate();
    
    console.log("Respuesta recibida de creacion de lista: "+createList);

    if(createList == true){
        console.log("Llamando a funcion encargada de verificar disponibilidad");
        var checkDates = await checkAvalaibleDate();
        if(checkDates == true){
          console.log("Hay disponibilidad, guardara los datos en la BD.");
          var confirm = await saveRoomsInHotel();
              if(confirm == true){
                globalThis.finalmessage = "Yay, we are ready!, See you soon :)";
               return true;
              } else {
                globalThis.finalmessage ="We are sorry :(. there was a problem trying to make the current reservation, so please check again later o visit our website.";
                return true;
              }
        } else {
          globalThis.finalmessage = "Sadly, the selected dates are already occupied. You can start again with another date, or call our stuff for get help.";
          return true;
        }
    } else {
      globalThis.finalmessage = "There was an internal problem, please try again later."
      return true;
    }
  }

  function getYesNoSimpleQuestion(question, yes, no) {
    return { 
        fulfillment_messages: [
      {
  
        "card": {
          "subtitle": question,
          "buttons": [
            {
              "text": yes,
              "postback": "Yes"
            },
            {
              "text": no,
              "postback": "no"
            }
          ]
        },
      },
    ]
  }
}

function setCardWithImageQuestion(title, subtitle,image,Positive, Negative){
  return {
    fulfillment_messages: [
      {
        "card": {
            "title": title,
            "subtitle": subtitle,
            "imageUri": image,
            "buttons": [
              {
                "text": Positive,
                "postback": "Yes"
              },
              {
                "text": Negative,
                "postback": "no"
              }
            ]
          },
      },
    ],
  };
}

function setCardWithImageOption(title, subtitle,image,Positive, Negative){
  return {
    fulfillment_messages: [
      {
        "card": {
            "title": title,
            "subtitle": subtitle,
            "imageUri": image,
            "buttons": [
              {
                "text": Positive,
                "postback": Positive
              },
              {
                "text": Negative,
                "postback": Negative
              }
            ]
          },
      },
    ],
  };
}

function setSimpleText(msg){
  return {
    fulfillmentMessages: [
    {
      "text": {
        "text": [msg]
      }
    }]
  }
}



