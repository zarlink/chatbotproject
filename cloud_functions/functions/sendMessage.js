const { DEFAULT_FAILURE_POLICY } = require("firebase-functions/v1")

async function getCards(products){

    let items = products.map((item) => {
        return {
            type: 'bubble',
            hero: {
                type: 'image',
                url: item.image,
                size: 'full',
                aspectRatio: '20:14',
                aspectMode: 'cover'
            },
            body: {
                type: 'box',
                layout: 'vertical',
                spacing: 'md',
                contents: [
                    {
                        type: 'text',
                        text: item.name,
                        size: 'md',
                        wrap: true,
                        align: 'center',
                    },
                    {
                        type: 'text',
                        text: item.price,
                        size: 'md',
                        align: 'center',
                    }
                ]
        
            },
            footer: {
                type: 'box',
                layout: 'horizontal',
                spacing: 'md',
                contents: [
                    {
                        type: 'button',
                        action: {
                            type: 'postback',
                            label: 'Choose',
                            data: 'action=buy&item=01',
                            text: item.name
                        },
                        color: '#87CBEEEE',
                        style: 'primary'
                    }
                    
                ]
            }
    
        }
    })

    return {
        type: 'flex',
        altText: 'List of products',
        contents: {
            type: 'carousel',
            contents: items
        }
    }

}


module.exports = {
    getCards
}